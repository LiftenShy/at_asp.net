On page google.com:

1. String for input keyword in search
   Css: #lst-ib
   XPATH: .//*[@id='lst-ib']

2. Button "Search in Google"
   CSS: [name=btnK]
   XPATH: .//*[@name='btnK']

3. Button "Luck me"
   CSS: [name=btnI]
   XPATH: .//*[@name='btnI']

On page result google:

1. Link on result search(all)
   CSS: h3.r > a
   XPATH: .//*[@class='r']/a

2. 5 word in Goooooooooogle (down, where pagination)
   CSS: a[href*="40"]
   XPATH: //a[@aria-label='Page 5']

On page yandex.com:

1. Login input field
   CSS: input[name="login"]
   XPATH: //input[@name='login']
 
2. Password input field
   CSS: input[name="passwd"]
   XPATH: //input[@name='passwd']

3. "Enter" button in login form
   CSS: button.auth__button
   XPATH: //button[contains(@class,'auth__button')]

On page yandex mail(who does not have mail - register)

1. Link "Inbox"
   CSS: a.ns-view-id-26
   XPATH: //a[contains(@class,'ns-view-id-26')]

2. Link "Sent"
   CSS: a.ns-view-id-27 use
   XPATH: //a[contains(@class,'ns-view-id-27')]

3. Link "Spam"
   CSS: a.ns-view-id-29 use
   XPATH: //a[contains(@class,'ns-view-id-29')]

4. Link "Trash"
   CSS: a.ns-view-id-28 use
   XPATH: //a[contains(@class,'ns-view-id-28')]

5. Link "Drafts"
   CSS: a.ns-view-id-30 use
   XPATH: //a[contains(@class,'ns-view-id-30')]

6. Button "Compose"
   CSS: a[href="#compose"]
   XPATH: .//a[@href='#compose']

7. button "Check mail"
   CSS: div[data-click-action="mailbox.check"]
   XPATH: .//div[@data-click-action='mailbox.check']

8. Buuton "Send" (On page Compose mail)
   CSS: [data-key="view=compose-send-button-complex"] button
   XPATH: //div[@data-key="view=compose-send-button-complex"]/button

9. Button "Mark as spam"
   CSS: div.ns-view-id-84
   XPATH: //div[contains(@class,'ns-view-id-84')]

10. Button "Read"
   CSS: div.ns-view-id-86
   XPATH: //div[contains(@class,'ns-view-id-86')]

11. Button "To Folder"
   CSS: div.ns-view-id-90
   XPATH: //div[contains(@class,'ns-view-id-90')]

12. Button "Pin"
   CSS: div.ns-view-id-91
   XPATH: //div[contains(@class,'ns-view-id-91')]

13. Selector for search unique mail
   CSS: a[href="#message/160440736725073920"]
   XPATH: //a[@href='#message/160440736725073920']

On page yandex disk

1. Button Upload
   CSS: input.button__attach
   XPATH: //input[@class='button__attach']

2. Selector for unique file on disk
   CSS: div.ns-view-resource[data-id*="Mountains"]
   XPATH: //div[@data-id='/disk/Bears.jpg']

3. Button Download
   CSS: button[data-click-action*="download"]
   XPATH: //button[@data-click-action='resource.download']

4. Button Delete
   CSS: button[data-click-action="resource.delete"]
   XPATH: //button[@data-click-action='resource.delete']

5. Button in Trush
   CSS: a[href="/client/trash"]
   XPATH: //a[@href='/client/trash']

6. Button restore file
   CSS: button[data-click-action="resource.restore"]
   XPATH: //button[@data-click-action='resource.restore']
