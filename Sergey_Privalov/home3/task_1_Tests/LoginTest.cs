﻿using NUnit.Framework;
using task_1.Models.Factory;
using task_1.Service;


namespace task_1_Tests
{
    [TestFixture]
    public class LoginTest : BaseTest
    {
        LoginService loginService = new LoginService();

        private const string expected_error_message_in_case_of_account_not_exist = "Нет аккаунта с таким логином.";
        private const string expected_error_message_in_case_of_wrong_password = "Неправильный логин или пароль.";
        private const string success_login_in_mail = "success";

        [Test]
        public void LoginTest_LoginWithCorrectAccount()
        {
            loginService.LoginToMailBox(AccountFactory.Account);
            string result = loginService.SuccessfulAuthorizationConfirmation();
            Assert.That(success_login_in_mail, Is.EqualTo(result));
        }

        [Test]
        public void LoginTest_LoginWithWrongLoginAndPassword()
        {
            loginService.LoginToMailBox(AccountFactory.NonExistedAccount);
            string errorMesage = loginService.RetrieveErrorOnFailedLogin();
            Assert.That(expected_error_message_in_case_of_account_not_exist, Is.EqualTo(errorMesage));
        }



        [Test]
        public void LoginTest_LoginWithWrongOnlyPassword()
        {
            loginService.LoginToMailBox(AccountFactory.AccountWithWrongPassword);
            string errorMesage = loginService.RetrieveErrorOnFailedLogin();
            Assert.That(expected_error_message_in_case_of_wrong_password, Is.EqualTo(errorMesage));
        }
    }
}
