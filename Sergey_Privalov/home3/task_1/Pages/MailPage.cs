﻿using System.Collections.ObjectModel;
using OpenQA.Selenium;
using task_1.Framework;

namespace task_1.Pages
{
    class MailPage
    {
        private Browser browser = Browser.Instance;

        private static readonly By ButtonComposeByCSS = By.CssSelector("[data-key*=compose-go]");
        private static readonly By InputFieldToByCSS = By.CssSelector("div[name=to]");
        private static readonly By InputFieldSubjectByCSS = By.CssSelector("input[name=subj]");
        private static readonly By TextBoxMailByCSS = By.CssSelector("div[role=textbox]");
        private static readonly By ButtonSendByCSS = By.CssSelector("button[type=submit]");
        private static readonly By ButtonCloseOnChangeByCSS = By.CssSelector("a._nb-popup-close");
        private static readonly By ButtonCheckMailByCSS = By.CssSelector("[data-key*=check-mail]");
        private static readonly By TitleFirstMailIByCSS = By.CssSelector("span[class*=Item_subject] span[title]");
        private static readonly By ButtonSentByCSS = By.CssSelector("a[href=\"#sent\"]");
        private static readonly By ButtonDeleteByCSS = By.CssSelector("div[data-key*=button-delete]");
        private static readonly By ButtonTrashByCSS = By.CssSelector("a[title*=Trash]");
        private static readonly By CheckBoxMailByCSS = By.CssSelector("label[data-nb=checkbox] > span[class*=checkbox-flag]");
        private static readonly By EmailMessageBoxByCSS = By.CssSelector("div[data-key*=item-box]");

        public IWebElement ButtonNewCompose { get { return browser.FindElement(ButtonComposeByCSS); } }
        public IWebElement InputTo { get { return browser.FindElement(InputFieldToByCSS); } }
        public IWebElement InputSubject { get { return browser.FindElement(InputFieldSubjectByCSS); } }
        public IWebElement InputBodyMail { get { return browser.FindElement(TextBoxMailByCSS); } }
        public IWebElement ButtonSendMail { get { return browser.FindElement(ButtonSendByCSS); } }
        public IWebElement ClosePopUpWindow { get { return browser.FindElement(ButtonCloseOnChangeByCSS); } }
        public IWebElement ButtonCheckMail { get { return browser.FindElement(ButtonCheckMailByCSS); } }
        public IWebElement GetTitleFirstMail { get { return browser.FindElement(TitleFirstMailIByCSS); } }
        public IWebElement ButtonMoveOnPageSent { get { return browser.FindElement(ButtonSentByCSS); } }
        public IWebElement ButtonDelete { get { return browser.FindElement(ButtonDeleteByCSS); } }
        public IWebElement ButtonMoveOnPageTrash { get { return browser.FindElement(ButtonTrashByCSS); } }
        public IWebElement SelectFirstMail { get { return browser.FindElement(CheckBoxMailByCSS); } }
        public ReadOnlyCollection<IWebElement> CountEmailMessage { get { return browser.FindElements(EmailMessageBoxByCSS); } }
    }
}
