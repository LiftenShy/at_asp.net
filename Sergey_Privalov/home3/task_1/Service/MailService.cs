﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using task_1.Models;
using task_1.Pages;

namespace task_1.Service
{
    public class MailService
    {
        private MailPage mailPage = new MailPage();

        public void SendMail(NewMail mail)
        {
            mailPage.ButtonNewCompose.Click();
            mailPage.InputTo.SendKeys(mail.SendTo);
            mailPage.InputSubject.SendKeys(mail.Subject);
            mailPage.InputBodyMail.SendKeys(mail.BodyMail);
            mailPage.ButtonSendMail.Click();
            try
            {
                mailPage.ClosePopUpWindow.Click();
                mailPage.ButtonCheckMail.Click();
            }
            catch (Exception)
            {
                mailPage.ButtonCheckMail.Click();
            }
            
        }

        public void SendMailWithWrongMail(NewMail mail)
        {
            mailPage.ButtonNewCompose.Click();
            mailPage.InputTo.SendKeys(mail.SendTo);
            mailPage.InputSubject.SendKeys(mail.Subject);
            mailPage.InputBodyMail.SendKeys(mail.BodyMail);
            mailPage.ButtonSendMail.Click();
        }

        public bool Check_Inbox_Sent_New_Mail()
        {
            string titleNewMailInbox = mailPage.GetTitleFirstMail.Text;
            mailPage.ButtonMoveOnPageSent.Click();
            string titleNewMailSent = mailPage.GetTitleFirstMail.Text;
            return titleNewMailInbox.Equals(titleNewMailSent);
        }

        public string RetrieveErrorOnFailedSendMail()
        {
            return new MailFailedPage().ErrorMessage.Text;
        }

        public int Get_Count_Mails()
        {
            return mailPage.CountEmailMessage.Count;
        }

        public void Delete_EmailMessage_And_Checkmail()
        {
            mailPage.SelectFirstMail.Click();
            mailPage.ButtonDelete.Click();
            mailPage.ButtonCheckMail.Click();
        }

        public void Move_On_Page_Trash()
        {
            mailPage.ButtonMoveOnPageTrash.Click();
        }
    }
}
