﻿using System;
using task_1.Models;
using task_1.Pages;

namespace task_1.Service
{
    public class LoginService
    {
        private MailLoginPage mailLoginPage = new MailLoginPage();

        public void LoginToMailBox(UserAccount account)
        {
            mailLoginPage.LoginInput.SendKeys(account.Login);
            mailLoginPage.PasswordInput.SendKeys(account.Password);
            mailLoginPage.SubmitButton.Click();
        }

        public string RetrieveErrorOnFailedLogin()
        {
            return new LoginFailedPage().ErrorMessage.Text;
        }

        public string SuccessfulAuthorizationConfirmation()
        {
            try
            {
                return  new LoginFailedPage().ErrorMessage.Text;
            }
            catch (Exception)
            {
                return "success";
            }
        }
    }
}
